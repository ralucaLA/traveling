//
//  HotelViewController.m
//  Traveling
//
//  Created by Raluca Radu on 11/28/16.
//  Copyright © 2016 ralucaradu. All rights reserved.
//

#import "HotelViewController.h"
#import "Hotel.h"
#import "Loading.h"
#import "UIImageView+AFNetworking.h"

#define PHOTO_SIZE 300
#define PHOTO_EXTRA 10

@interface HotelViewController ()

@end

@implementation HotelViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    theHotel = [[Loading sharedInstance] getAHotel];
    
    [self.nameLabel setText:theHotel.name];
    [self.descriptionLabel setText:theHotel.hotelDescription];
    [self.locationLabel setText:theHotel.hotel_location];
    
    NSString *facilities = @"";
    for (NSString *one in theHotel.facilities) {
        if ([facilities length] == 0) {
            facilities = [NSString stringWithFormat:@"%@", one];
        } else {
            facilities = [NSString stringWithFormat:@"%@, %@", facilities, one];
        }
    }
    [self.facilitiesLabel setText:facilities];
    
    x = 0;
    for (NSString *link in theHotel.images) {
        if ([link length] != 0) {
            UIImageView *anImageView = [[UIImageView alloc] init];
            
            [anImageView setImageWithURL:[NSURL URLWithString:link]];
            [anImageView setFrame:CGRectMake(x, 0, PHOTO_SIZE, PHOTO_SIZE)];
            [self.photosScrollView addSubview:anImageView];
            x = x + anImageView.frame.size.width + PHOTO_EXTRA;
        }
    }
    
    self.photosScrollView.pagingEnabled = NO;
    self.photosScrollView.showsHorizontalScrollIndicator = NO;
    self.photosScrollView.showsVerticalScrollIndicator = NO;
    self.photosScrollView.scrollsToTop = NO;
    self.photosScrollView.delegate = self;
    [self.photosScrollView setContentSize:CGSizeMake(x - 10, self.photosScrollView.frame.size.height)];
    
    if ([theHotel.images count] >= 3) {
        int off = PHOTO_SIZE - (self.view.frame.size.width - PHOTO_SIZE - 2 * PHOTO_EXTRA) / 2;
        [self.photosScrollView setContentOffset:CGPointMake(off, 0) animated:NO];
    }
    
    [self.ratingView addSubview:[self getStarView:[theHotel.rating floatValue]]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (UIView*) getStarView:(float)rating
{
    UIImage *star_white = [UIImage imageNamed:@"star_white"];
    UIImage *star_black = [UIImage imageNamed:@"star_black"];
    UIImage *star_half = [UIImage imageNamed:@"star_half"];
    
    float height = MAX(star_white.size.height, star_black.size.height);
    height = MAX(height, star_half.size.height);
    int starOffsetY = (30 - (int)height) / 2;
    height = MAX((int)height+1, 30);
    
    float width = MAX(star_white.size.width, star_black.size.width);
    width = MAX(width, star_half.size.width);
    width = (int)width + 1;
    
    float spacingX = 2;
    
    UIView *starView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, (width + spacingX) * 5, height)];
    starView.backgroundColor = [UIColor clearColor];
    
    __block float starX = 0;
    int i = 0;
    
    void (^addStar)(id) = ^(UIImage *star)
    {
        UIImageView *tmp = [[UIImageView alloc] initWithImage:star];
        tmp.frame = CGRectMake(starX, starOffsetY, star.size.width, star.size.height);
        starX += star.size.width + spacingX;
        [starView addSubview:tmp];
    };
    
    for(; i < (int)rating; i++)
        addStar(star_white);
    
    if(i < rating)
    {
        addStar(star_half);
        i++;
    }
    
    for(; i < 5; i++)
        addStar(star_black);
    
    return starView;
}
@end
