//
//  FlightsViewController.h
//  Traveling
//
//  Created by Raluca Radu on 11/26/16.
//  Copyright © 2016 ralucaradu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FlightsViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

    @property (weak, nonatomic) IBOutlet UITableView *flightsTableView;
    
@end
