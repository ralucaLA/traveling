//
//  Flight.m
//  Traveling
//
//  Created by Raluca Radu on 11/26/16.
//  Copyright © 2016 ralucaradu. All rights reserved.
//

#import "Flight.h"

@implementation Flight

    - (BOOL) hasInfo {
        return self.hasInfoState;
    }
    
    - (void) updateInfoWithDictionary:(NSDictionary*)dictionary {
        int count = 0;
        if (dictionary && [dictionary count] != 0) {
            if ([dictionary objectForKey:@"airline"]) {
                self.airline = [dictionary objectForKey:@"airline"];
                count++;
            }
            if ([dictionary objectForKey:@"departure_date"]) {
                NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                [dateFormat setDateFormat:@"YYYY-MM-dd'T'HH:mm:ss'Z'"];
                NSDate *dte = [dateFormat dateFromString:[dictionary objectForKey:@"departure_date"]];
                self.departure_date = dte;
                count++;
            }
            if ([dictionary objectForKey:@"arrival_date"]) {
                NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                [dateFormat setDateFormat:@"YYYY-MM-dd'T'HH:mm:ss'Z'"];
                NSDate *dte = [dateFormat dateFromString:[dictionary objectForKey:@"arrival_date"]];
                self.arrival_date = dte;
                count++;
            }
            if ([dictionary objectForKey:@"price"]) {
                self.price = [NSNumber numberWithInteger:[[dictionary objectForKey:@"price"] intValue]];
                count++;
            }
            if ([dictionary objectForKey:@"departure_airport"]) {
                self.departure_airport = [dictionary objectForKey:@"departure_airport"];
                count++;
            }
            if ([dictionary objectForKey:@"arrival_airport"]) {
                self.arrival_airport = [dictionary objectForKey:@"arrival_airport"];
                count++;
            }
        }
        
        if (count == 6) {
            [self setHasInfoState:YES];
        }
    }
    
    - (void) deleteFlight {
        
        [self setHasInfoState:NO];
    }
@end
