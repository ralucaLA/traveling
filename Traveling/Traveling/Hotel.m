//
//  Hotel.m
//  Traveling
//
//  Created by Raluca Radu on 11/28/16.
//  Copyright © 2016 ralucaradu. All rights reserved.
//

#import "Hotel.h"

@implementation Hotel

    - (BOOL) hasInfo {
        return self.hasInfoState;
    }

    - (void) updateInfoWithDictionary:(NSDictionary*)dictionary {
        int count = 0;
        if (dictionary && [dictionary count] != 0) {
            if ([dictionary objectForKey:@"name"]) {
                self.name = [dictionary objectForKey:@"name"];
                count++;
            }
            
            if ([dictionary objectForKey:@"hotel_location"]) {
                self.hotel_location = [dictionary objectForKey:@"hotel_location"];
                count++;
            }
            
            if ([dictionary objectForKey:@"description"]) {
                self.hotelDescription = [dictionary objectForKey:@"description"];
                count++;
            }
            
            if ([dictionary objectForKey:@"images"]) {
                self.images = [dictionary objectForKey:@"images"];
                count++;
            }

            if ([dictionary objectForKey:@"rating"]) {
                self.rating = [NSNumber numberWithFloat:[[dictionary objectForKey:@"rating"] floatValue]];
                count++;
            }
        
            if ([dictionary objectForKey:@"facilities"]) {
                self.facilities = [dictionary objectForKey:@"facilities"];
                count++;
            }
        }
    
        if (count == 6) {
            [self setHasInfoState:YES];
        }
    }

    - (void) deleteHotel {
        [self setHasInfoState:NO];
    }


@end
