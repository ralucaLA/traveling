//
//  Hotel.h
//  Traveling
//
//  Created by Raluca Radu on 11/28/16.
//  Copyright © 2016 ralucaradu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Hotel : NSObject

    @property (nonatomic, assign) BOOL hasInfoState;

    @property (nonatomic, strong) NSString  *name;
    @property (nonatomic, strong) NSString  *hotel_location;
    @property (nonatomic, strong) NSString  *hotelDescription;
    @property (nonatomic, strong) NSArray   *images;
    @property (nonatomic, strong) NSNumber  *rating;
    @property (nonatomic, strong) NSArray   *facilities;

    - (BOOL) hasInfo;
    - (void) updateInfoWithDictionary:(NSDictionary*)dictionary;
    - (void) deleteHotel;

@end
