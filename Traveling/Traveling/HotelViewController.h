//
//  HotelViewController.h
//  Traveling
//
//  Created by Raluca Radu on 11/28/16.
//  Copyright © 2016 ralucaradu. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Hotel;

@interface HotelViewController : UIViewController <UIScrollViewDelegate> {
    Hotel *theHotel;
    int x;
}

@property (weak, nonatomic) IBOutlet UIScrollView *photosScrollView;
@property (weak, nonatomic) IBOutlet UIButton *leftArrow;
@property (weak, nonatomic) IBOutlet UIButton *rightArrow;

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;
@property (weak, nonatomic) IBOutlet UILabel *facilitiesLabel;
@property (weak, nonatomic) IBOutlet UIView *ratingView;

@end
