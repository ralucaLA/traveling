//
//  Loading.m
//  Traveling
//
//  Created by Raluca Radu on 11/26/16.
//  Copyright © 2016 ralucaradu. All rights reserved.
//

#import "Loading.h"
#import "Flight.h"
#import "Hotel.h"

@implementation Loading

    + (id) sharedInstance {
        static Loading *sharedInstance = nil;
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            sharedInstance = [[self alloc] init];
        });
        return sharedInstance;
    }
    
- (id) init {
    if (self = [super init]) {
        // create session
        manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[[NSURL alloc] initWithString:API_URL]];
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        //this ensures that no more than the maximum number of requests specified will be executed at the same time.
        manager.operationQueue.maxConcurrentOperationCount = MAXIMUM_CONCURENT_REQUESTS;
        //this ensures that requests are performed in the background
        manager.operationQueue.qualityOfService = NSQualityOfServiceDefault;
        //this ensures that responses come in the background - the thread the response be received is different from the main.
        manager.completionQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
        allFlights = [[NSMutableArray alloc] init];
        aHotel = [[Hotel alloc] init];
    }
    return self;
}

#pragma mark API CALLS
    - (void) getFlightsWithCompletionBlock:(ResultCompletionBlock)completionBlock {
        
        [manager GET:@"bFnZQEx0" parameters:nil
             success:^(NSURLSessionDataTask *task, id responseObject) {
                 NSError *error = nil;
                 NSDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
                 if (!error) {
                     NSLog(@"responseDictionary: %@", responseDictionary);
                     
                     NSArray *flightsArray = [responseDictionary objectForKey:@"flights"];
                     for (int i = 0; i < [flightsArray count]; i++) {
                         NSDictionary *aFlight = [flightsArray objectAtIndex:i];
                         Flight *flight = [[Flight alloc] init];
                         [flight updateInfoWithDictionary:aFlight];
                         [allFlights addObject:flight];
                     }
                     
                     if (completionBlock)
                        completionBlock(nil, nil, @"DONE");
                     
                     [[NSNotificationCenter defaultCenter] postNotificationName:@"DOWNLOAD_FLIGHTS" object:nil];
                 }
                 else {
                     if (completionBlock)
                        completionBlock(@"parse error", error.localizedDescription, nil);
                 }
             }
             failure:^(NSURLSessionDataTask *task, NSError *error) {
                 NSLog(@"getFlights error: %@", error.description);
                 if (completionBlock)
                    completionBlock(@"call error", error.localizedDescription, nil);
             }
         ];
    }

- (void) getAHotelWithCompletionBlock:(ResultCompletionBlock)completionBlock {
    
    [manager GET:@"f0Tm6bfy" parameters:nil
         success:^(NSURLSessionDataTask *task, id responseObject) {
             NSError *error = nil;
             NSDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
             if (!error) {
                 NSLog(@"responseDictionary: %@", responseDictionary);
                 
                 if (responseDictionary && [responseDictionary count] != 0) {
                     Hotel *hotel = [[Hotel alloc] init];
                     [hotel updateInfoWithDictionary:responseDictionary];
                     aHotel = hotel;
                 }
                 
                 if (completionBlock)
                     completionBlock(nil, nil, @"DONE");
                 
                 [[NSNotificationCenter defaultCenter] postNotificationName:@"DOWNLOAD_A_HOTEL" object:nil];
             }
             else {
                 if (completionBlock)
                     completionBlock(@"parse error", error.localizedDescription, nil);
             }
         }
         failure:^(NSURLSessionDataTask *task, NSError *error) {
             NSLog(@"getAHotel error: %@", error.description);
             if (completionBlock)
                 completionBlock(@"call error", error.localizedDescription, nil);
         }
     ];
}

#pragma mark STORAGE
    - (NSMutableArray*) getAllFlights {
        return allFlights;
    }

    - (Hotel*) getAHotel {
        return aHotel;
    }
@end
