//
//  Flight.h
//  Traveling
//
//  Created by Raluca Radu on 11/26/16.
//  Copyright © 2016 ralucaradu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Flight : NSObject
    
    @property (nonatomic, assign) BOOL hasInfoState;
    
    @property (nonatomic, strong) NSString *airline;
    @property (nonatomic, strong) NSDate *departure_date;
    @property (nonatomic, strong) NSDate *arrival_date;
    @property (nonatomic, strong) NSNumber *price;
    @property (nonatomic, strong) NSString *departure_airport;
    @property (nonatomic, strong) NSString *arrival_airport;

    - (BOOL) hasInfo;
    - (void) updateInfoWithDictionary:(NSDictionary*)dictionary;
    - (void) deleteFlight;
@end
