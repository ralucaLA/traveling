//
//  Loading.h
//  Traveling
//
//  Created by Raluca Radu on 11/26/16.
//  Copyright © 2016 ralucaradu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFHTTPSessionManager.h"

#define MAXIMUM_CONCURENT_REQUESTS 5
#define API_URL @"http://pastebin.com/raw/"


@class Hotel;

@interface Loading : NSObject {
    AFHTTPSessionManager *manager;
    
    NSMutableArray  *allFlights;
    Hotel           *aHotel;
}
    
    typedef void (^ResultCompletionBlock)(NSString *errorType, NSString *errorMessage, id dataResult);
    

    + (id)sharedInstance;
    
    - (void) getFlightsWithCompletionBlock:(ResultCompletionBlock)completionBlock;
    - (void) getAHotelWithCompletionBlock:(ResultCompletionBlock)completionBlock;

    - (NSMutableArray*) getAllFlights;
    - (Hotel*) getAHotel;
    
@end
