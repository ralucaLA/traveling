//
//  FlightsViewController.m
//  Traveling
//
//  Created by Raluca Radu on 11/26/16.
//  Copyright © 2016 ralucaradu. All rights reserved.
//

#import "FlightsViewController.h"
#import "Loading.h"
#import "Flight.h"
#import "FlightTableViewCell.h"

@interface FlightsViewController ()

@end

@implementation FlightsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
    
    - (void) viewWillAppear:(BOOL)animated {
        [super viewWillAppear:animated];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadTable) name:@"DOWNLOAD_FLIGHTS" object:nil];
    }
    
    - (void) viewWillDisappear:(BOOL)animated {
        [super viewWillDisappear:animated];
        
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"DOWNLOAD_FLIGHTS" object:nil];
    }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
    
    - (void) reloadTable {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.flightsTableView reloadData];
        });
    }
    
#pragma mark - Table view data source
    
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
    
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[[Loading sharedInstance] getAllFlights] count];
}
    
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    FlightTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"flightIdentifier" forIndexPath:indexPath];
    if (cell == nil) {
        cell = [[FlightTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"flightIdentifier"];
    }
    
    Flight *aFlight = [[[Loading sharedInstance] getAllFlights] objectAtIndex:indexPath.row];
    [cell.departureCityLabel setText:aFlight.departure_airport];
    [cell.arrivalCityLabel setText:aFlight.arrival_airport];
    [cell.airlineLabel setText:aFlight.airline];
    [cell.priceLabel setText:[NSString stringWithFormat:@"€ %d", [aFlight.price intValue]]];
    
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"dd, MMMM, yyyy (EEEE) HH:mm z"];
    
    [cell.departureDateLabel setText:[format stringFromDate:aFlight.departure_date]];
    [cell.arrivalDateLabel setText:[format stringFromDate:aFlight.arrival_date]];
    
    return cell;
}
    
#pragma mark - Table view delegate
    
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}


@end
