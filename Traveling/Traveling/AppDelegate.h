//
//  AppDelegate.h
//  Traveling
//
//  Created by Raluca Radu on 11/26/16.
//  Copyright © 2016 ralucaradu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate> {
    UIStoryboard            *storyboard;
}

@property (strong, nonatomic) UIWindow *window;


@end

