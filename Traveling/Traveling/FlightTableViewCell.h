//
//  FlightTableViewCell.h
//  Traveling
//
//  Created by Raluca Radu on 11/26/16.
//  Copyright © 2016 ralucaradu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FlightTableViewCell : UITableViewCell
    @property (weak, nonatomic) IBOutlet UILabel *departureCityLabel;
    @property (weak, nonatomic) IBOutlet UILabel *arrivalCityLabel;
    @property (weak, nonatomic) IBOutlet UILabel *airlineLabel;
    @property (weak, nonatomic) IBOutlet UILabel *departureDateLabel;
    @property (weak, nonatomic) IBOutlet UILabel *arrivalDateLabel;
    @property (weak, nonatomic) IBOutlet UILabel *priceLabel;
    
@end
