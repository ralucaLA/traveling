//
//  LoadingSpec.m
//  Traveling
//
//  Created by Raluca Radu on 11/26/16.
//  Copyright © 2016 ralucaradu. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <Specta/Specta.h>
#define EXP_SHORTHAND
#import "Expecta.h"

#import "Loading.h"
#import "Hotel.h"


SpecBegin(Loading)
describe(@"Loading", ^{
    
    it(@"should get flights on launch", ^{
        [[Loading sharedInstance] getFlightsWithCompletionBlock:^(NSString *errorType, NSString *errorMessage, id dataResult) {
            expect([[[Loading sharedInstance] getAllFlights] count]).toNot.equal(0);
        }];
    });
    
    it(@"should get a hotel on launch", ^{
        [[Loading sharedInstance] getAHotelWithCompletionBlock:^(NSString *errorType, NSString *errorMessage, id dataResult) {
            expect([[[Loading sharedInstance] getAHotel] hasInfo]).toNot.equal(NO);
        }];
    });
    
   
});
SpecEnd
