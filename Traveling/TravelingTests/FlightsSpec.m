//
//  FlightsSpec.m
//  Traveling
//
//  Created by Raluca Radu on 11/26/16.
//  Copyright © 2016 ralucaradu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Specta/Specta.h>
#define EXP_SHORTHAND
#import "Expecta.h"
#import "Flight.h"


SpecBegin(Flight)
describe(@"Flight", ^{
    Flight *flight = [[Flight alloc] init];
    
    it(@"has info on creation", ^{
        expect([flight hasInfo]).to.equal(NO);
    });
    
    it(@"is updating when callback is in", ^{
        NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
        [dictionary setObject:@"FastJet" forKey:@"airline"];
        [dictionary setObject:@"2016-10-20T10:00:00Z" forKey:@"departure_date"];
        [dictionary setObject:@"2016-10-20T11:00:00Z" forKey:@"arrival_date"];
        [dictionary setObject:@"12300" forKey:@"price"];
        [dictionary setObject:@"London Gatwick" forKey:@"departure_airport"];
        [dictionary setObject:@"Barcelona" forKey:@"arrival_airport"];

        [flight updateInfoWithDictionary:dictionary];
        expect([flight hasInfo]).to.equal(YES);
    });
    
    it(@"has no info when it's deleted", ^{
        [flight deleteFlight];
        expect([flight hasInfo]).to.equal(NO);
    });
});
SpecEnd
