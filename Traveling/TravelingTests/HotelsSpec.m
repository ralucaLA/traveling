//
//  HotelsSpec.m
//  Traveling
//
//  Created by Raluca Radu on 11/28/16.
//  Copyright © 2016 ralucaradu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Specta/Specta.h>
#define EXP_SHORTHAND
#import "Expecta.h"
#import "Hotel.h"


SpecBegin(Hotel)
describe(@"Hotel", ^{
    Hotel *hotel = [[Hotel alloc] init];
    
    it(@"has info on creation", ^{
        expect([hotel hasInfo]).to.equal(NO);
    });
    
    it(@"is updating when callback is in", ^{
        NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
        [dictionary setObject:@"Labranda Isla Bonita Hotel" forKey:@"name"];
        [dictionary setObject:@"Costa Adeje, Tenerife, Canaries" forKey:@"hotel_location"];
        [dictionary setObject:@"On the Beach Offer - Save up to 15%\r\nBook this hotel by 31.10.2016, for travel between 01.11.2016 and 30.04.2017. Offer applicable to all room types on any board basis." forKey:@"description"];
        NSArray *images = [NSArray arrayWithObjects:@"https://i.onthebeach.co.uk/v1/hotel_images/1ba1aa1c-17e4-4717-afe7-f2b2ea877b87/cover/374/296/high/1.0/labranda-isla-bonita-hotel.jpeg", @"https://i.onthebeach.co.uk/v1/hotel_images/73f82009-b0f5-43b6-9eee-cb0fb25f459a/cover/374/296/high/1.0/labranda-isla-bonita-hotel.jpeg", @"https://i.onthebeach.co.uk/v1/hotel_images/d26659fb-e513-4fbf-9fed-2c24583ad4f0/cover/374/296/high/1.0/labranda-isla-bonita-hotel.jpeg", nil];
        [dictionary setObject:images forKey:@"images"];
        [dictionary setObject:@"4" forKey:@"rating"];
        NSArray *facilities = [NSArray arrayWithObjects:@"24 Hour Reception", @"Aerobics", @"Air Conditioning", nil];
        [dictionary setObject:facilities forKey:@"facilities"];
        
        [hotel updateInfoWithDictionary:dictionary];
        expect([hotel hasInfo]).to.equal(YES);
    });
    
    it(@"has no info when it's deleted", ^{
        [hotel deleteHotel];
        expect([hotel hasInfo]).to.equal(NO);
    });
});
SpecEnd
